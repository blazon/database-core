# Blazon Database Core
This package contains the database integration for the Doctrine ORM and the Blazon API framework.

#### Table of Contents
- [Installation](#installation)
- [Usage](#usage)
- [Configuration](#configuration)
    - [Minimal Configuration](#minimal-configuration)
    - [Full Configuration](#full-configuration)
- [Upgrades](#upgrades)    


# Installation

```bash
composer require blazon/database-core
```

# Usage

# Configuration

## Minimal Configuration

### Example 

## Full Configuration

### Example

### Upgrades
