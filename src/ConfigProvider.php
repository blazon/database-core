<?php

declare(strict_types=1);

namespace Blazon\DatabaseCore;

use Blazon\DatabaseCore\Console\LoadDataFixturesDoctrineCommand;
use Blazon\DatabaseCore\Console\LoadDataFixturesDoctrineCommandFactory;
use Blazon\DatabaseCore\Service\EntityManagerHelperFactory;
use Blazon\DatabaseCore\Service\FixtureLoaderFactory;
use Blazon\DatabaseCore\Service\RepositoryFactory;
use Blazon\DatabaseCore\Service\ResolveTargetEntityListenerFactory;
use Doctrine\Common\DataFixtures\Loader;
use Doctrine\DBAL\Tools\Console as DBALConsole;
use Doctrine\Migrations\Configuration\Migration\ConfigurationLoader;
use Doctrine\Migrations\DependencyFactory;
use Doctrine\Migrations\Tools\Console as MigrationConsole;
use Doctrine\ORM\Tools\Console;
use Doctrine\ORM\Tools\Console\Helper\EntityManagerHelper;
use Doctrine\ORM\Tools\ResolveTargetEntityListener;
use Roave\PsrContainerDoctrine\Migrations\CommandFactory;
use Roave\PsrContainerDoctrine\Migrations\ConfigurationLoaderFactory;
use Roave\PsrContainerDoctrine\Migrations\DependencyFactoryFactory;
use Symfony\Component\Console\Helper\QuestionHelper;

/** @SuppressWarnings(PHPMD.CouplingBetweenObjects)  */
class ConfigProvider
{
    public function __invoke(): array
    {
        return [
            'doctrine' => [
                'configuration' => [
                    'orm_default' => [
                        'repository_factory' => RepositoryFactory::class
                    ],
                ],

                'resolveTargetEntities' => []
            ],

            'console' => [
                'commands' => [
                    DBALConsole\Command\ImportCommand::class
                        => DBALConsole\Command\ImportCommand::class,
                    DBALConsole\Command\ReservedWordsCommand:: class
                        => DBALConsole\Command\ReservedWordsCommand::class,
                    DBALConsole\Command\RunSqlCommand::class
                        => DBALConsole\Command\RunSqlCommand::class,
                    Console\Command\ClearCache\CollectionRegionCommand::class
                        => Console\Command\ClearCache\CollectionRegionCommand::class,
                    Console\Command\ClearCache\EntityRegionCommand::class
                        => Console\Command\ClearCache\EntityRegionCommand::class,
                    Console\Command\ClearCache\MetadataCommand::class
                        => Console\Command\ClearCache\MetadataCommand::class,
                    Console\Command\ClearCache\QueryCommand::class
                        => Console\Command\ClearCache\QueryCommand::class,
                    Console\Command\ClearCache\QueryRegionCommand::class
                        => Console\Command\ClearCache\QueryRegionCommand::class,
                    Console\Command\ClearCache\ResultCommand::class
                        => Console\Command\ClearCache\ResultCommand::class,
                    Console\Command\SchemaTool\CreateCommand::class
                        => Console\Command\SchemaTool\CreateCommand::class,
                    Console\Command\SchemaTool\UpdateCommand::class
                        => Console\Command\SchemaTool\UpdateCommand::class,
                    Console\Command\SchemaTool\DropCommand::class
                        => Console\Command\SchemaTool\DropCommand::class,
                    Console\Command\EnsureProductionSettingsCommand::class
                        => Console\Command\EnsureProductionSettingsCommand::class,
                    Console\Command\ConvertDoctrine1SchemaCommand::class
                        => Console\Command\ConvertDoctrine1SchemaCommand::class,
                    Console\Command\GenerateRepositoriesCommand::class
                        => Console\Command\GenerateRepositoriesCommand::class,
                    Console\Command\GenerateEntitiesCommand::class
                        => Console\Command\GenerateEntitiesCommand::class,
                    Console\Command\GenerateProxiesCommand::class
                        => Console\Command\GenerateProxiesCommand::class,
                    Console\Command\ConvertMappingCommand::class
                        => Console\Command\ConvertMappingCommand::class,
                    Console\Command\RunDqlCommand::class
                        => Console\Command\RunDqlCommand::class,
                    Console\Command\ValidateSchemaCommand::class
                        => Console\Command\ValidateSchemaCommand::class,
                    Console\Command\InfoCommand::class
                        => Console\Command\InfoCommand::class,
                    Console\Command\MappingDescribeCommand::class
                        => Console\Command\MappingDescribeCommand::class,

                    MigrationConsole\Command\DumpSchemaCommand::class
                        => MigrationConsole\Command\DumpSchemaCommand::class,
                    MigrationConsole\Command\ExecuteCommand::class
                        => MigrationConsole\Command\ExecuteCommand::class,
                    MigrationConsole\Command\GenerateCommand::class
                        => MigrationConsole\Command\GenerateCommand::class,
                    MigrationConsole\Command\LatestCommand::class
                        => MigrationConsole\Command\LatestCommand::class,
                    MigrationConsole\Command\MigrateCommand::class
                        => MigrationConsole\Command\MigrateCommand::class,
                    MigrationConsole\Command\RollupCommand::class
                        => MigrationConsole\Command\RollupCommand::class,
                    MigrationConsole\Command\StatusCommand::class
                        => MigrationConsole\Command\StatusCommand::class,
                    MigrationConsole\Command\VersionCommand::class
                        => MigrationConsole\Command\VersionCommand::class,
                    MigrationConsole\Command\UpToDateCommand::class
                        => MigrationConsole\Command\UpToDateCommand::class,
                    MigrationConsole\Command\DiffCommand::class
                        => MigrationConsole\Command\DiffCommand::class,

                    LoadDataFixturesDoctrineCommand::class
                        => LoadDataFixturesDoctrineCommand::class,
                ],

                'helpers' => [
                    'em' => EntityManagerHelper::class,
                    'question' => QuestionHelper::class
                ],
            ],
            'dependencies' => $this->getDependencies(),
        ];
    }

    /**
     * Returns the container dependencies
     */
    public function getDependencies(): array
    {
        return [
            'invokables' => [
                QuestionHelper::class => QuestionHelper::class,
                DBALConsole\Command\ImportCommand::class
                    => DBALConsole\Command\ImportCommand::class,
                DBALConsole\Command\ReservedWordsCommand:: class
                    => DBALConsole\Command\ReservedWordsCommand::class,
                DBALConsole\Command\RunSqlCommand::class
                    => DBALConsole\Command\RunSqlCommand::class,
                Console\Command\ClearCache\CollectionRegionCommand::class
                    => Console\Command\ClearCache\CollectionRegionCommand::class,
                Console\Command\ClearCache\EntityRegionCommand::class
                    => Console\Command\ClearCache\EntityRegionCommand::class,
                Console\Command\ClearCache\MetadataCommand::class
                    => Console\Command\ClearCache\MetadataCommand::class,
                Console\Command\ClearCache\QueryCommand::class
                    => Console\Command\ClearCache\QueryCommand::class,
                Console\Command\ClearCache\QueryRegionCommand::class
                    => Console\Command\ClearCache\QueryRegionCommand::class,
                Console\Command\ClearCache\ResultCommand::class
                    => Console\Command\ClearCache\ResultCommand::class,
                Console\Command\SchemaTool\CreateCommand::class
                    => Console\Command\SchemaTool\CreateCommand::class,
                Console\Command\SchemaTool\UpdateCommand::class
                    => Console\Command\SchemaTool\UpdateCommand::class,
                Console\Command\SchemaTool\DropCommand::class
                    => Console\Command\SchemaTool\DropCommand::class,
                Console\Command\EnsureProductionSettingsCommand::class
                    => Console\Command\EnsureProductionSettingsCommand::class,
                Console\Command\ConvertDoctrine1SchemaCommand::class
                    => Console\Command\ConvertDoctrine1SchemaCommand::class,
                Console\Command\GenerateRepositoriesCommand::class
                    => Console\Command\GenerateRepositoriesCommand::class,
                Console\Command\GenerateEntitiesCommand::class
                    => Console\Command\GenerateEntitiesCommand::class,
                Console\Command\GenerateProxiesCommand::class
                    => Console\Command\GenerateProxiesCommand::class,
                Console\Command\ConvertMappingCommand::class
                    => Console\Command\ConvertMappingCommand::class,
                Console\Command\RunDqlCommand::class
                    => Console\Command\RunDqlCommand::class,
                Console\Command\ValidateSchemaCommand::class
                    => Console\Command\ValidateSchemaCommand::class,
                Console\Command\InfoCommand::class
                    => Console\Command\InfoCommand::class,
                Console\Command\MappingDescribeCommand::class
                    => Console\Command\MappingDescribeCommand::class,

            ],
            'factories'  => [
                EntityManagerHelper::class => EntityManagerHelperFactory::class,
                Loader::class => FixtureLoaderFactory::class,
                LoadDataFixturesDoctrineCommand::class => LoadDataFixturesDoctrineCommandFactory::class,
                ResolveTargetEntityListener::class => ResolveTargetEntityListenerFactory::class,
                RepositoryFactory::class => RepositoryFactory::class,
                ConfigurationLoader::class => ConfigurationLoaderFactory::class,
                DependencyFactory::class => DependencyFactoryFactory::class,

                MigrationConsole\Command\DumpSchemaCommand::class
                    => CommandFactory::class,
                MigrationConsole\Command\ExecuteCommand::class
                    => CommandFactory::class,
                MigrationConsole\Command\GenerateCommand::class
                    => CommandFactory::class,
                MigrationConsole\Command\LatestCommand::class
                    => CommandFactory::class,
                MigrationConsole\Command\MigrateCommand::class
                    => CommandFactory::class,
                MigrationConsole\Command\RollupCommand::class
                    => CommandFactory::class,
                MigrationConsole\Command\StatusCommand::class
                    => CommandFactory::class,
                MigrationConsole\Command\VersionCommand::class
                    => CommandFactory::class,
                MigrationConsole\Command\UpToDateCommand::class
                    => CommandFactory::class,
                MigrationConsole\Command\DiffCommand::class
                    => CommandFactory::class,
            ],
        ];
    }
}
