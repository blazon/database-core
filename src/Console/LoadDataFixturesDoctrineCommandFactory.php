<?php

declare(strict_types=1);

namespace Blazon\DatabaseCore\Console;

use Psr\Container\ContainerInterface;

class LoadDataFixturesDoctrineCommandFactory
{
    public function __invoke(ContainerInterface $container)
    {
        return new LoadDataFixturesDoctrineCommand($container);
    }
}
