<?php

/**
 * Copyright (c) 2011 Fabien Potencier, Doctrine Project
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

declare(strict_types=1);

namespace Blazon\DatabaseCore\Console;

use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Loader;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\DBAL\Sharding\PoolingShardConnection;
use Doctrine\ORM\EntityManagerInterface;
use LogicException;
use Psr\Container\ContainerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

use function sprintf;

class LoadDataFixturesDoctrineCommand extends Command
{
    private ContainerInterface $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('fixtures:load')
            ->setDescription('Load data fixtures to your database')
            ->addOption(
                'append',
                null,
                InputOption::VALUE_NONE,
                'Append the data fixtures instead of deleting all data from the database first.'
            )->addOption(
                'em',
                null,
                InputOption::VALUE_REQUIRED,
                'The entity manager to use for this command.',
                'doctrine.entity_manager.orm_default'
            )->addOption(
                'shard',
                null,
                InputOption::VALUE_REQUIRED,
                'The shard connection to use for this command.'
            )->addOption(
                'purge-with-truncate',
                null,
                InputOption::VALUE_NONE,
                'Purge data by using a database-level TRUNCATE statement'
            )->setHelp(<<<EOT
The <info>%command.name%</info> command loads data fixtures from your application:

  <info>php %command.full_name%</info>

Fixtures are services that are tagged with <comment>doctrine.fixture.orm</comment>.

If you want to append the fixtures instead of flushing the database first you can use the <comment>--append</comment> 
option:

  <info>php %command.full_name%</info> <comment>--append</comment>

By default Doctrine Data Fixtures uses DELETE statements to drop the existing rows from the database.
If you want to use a TRUNCATE statement instead you can use the <comment>--purge-with-truncate</comment> flag:

  <info>php %command.full_name%</info> <comment>--purge-with-truncate</comment>

To execute only fixtures that live in a certain group, use:

  <info>php %command.full_name%</info> <comment>--group=group1</comment>

EOT
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $userInterface = $this->getSymfonyStyle($input, $output);
        $entityManager = $this->getEntityManager($input->getOption('em'));

        if (!$input->getOption('append')) {
            $confirm = $userInterface->confirm(
                sprintf(
                    'Careful, database "%s" will be purged. Do you want to continue?',
                    $entityManager->getConnection()->getDatabase()
                ),
                !$input->isInteractive()
            );

            if (!$confirm) {
                return 0;
            }
        }

        if ($input->getOption('shard')) {
            $connection = $entityManager->getConnection();

            if (!$connection instanceof PoolingShardConnection) {
                throw new LogicException(sprintf(
                    'Connection of EntityManager "%s" must implement shards configuration.',
                    $input->getOption('em')
                ));
            }

            $connection->connect($input->getOption('shard'));
        }

        $fixtures = $this->getLoader()->getFixtures();

        if (!$fixtures) {
            $message = 'Could not find any fixture services to load';
            $userInterface->error($message . '.');

            return 1;
        }

        $purgeMode = ORMPurger::PURGE_MODE_DELETE;
        if ($input->getOption('purge-with-truncate')) {
            $purgeMode = ORMPurger::PURGE_MODE_TRUNCATE;
        }

        $purger = $this->getORMPurger($entityManager);
        $purger->setPurgeMode($purgeMode);

        $executor = $this->getExecutor($entityManager, $purger);
        $executor->setLogger(static function ($message) use ($userInterface): void {
            $userInterface->text(sprintf('  <comment>></comment> <info>%s</info>', $message));
        });
        $executor->execute($fixtures, $input->getOption('append'));

        return 0;
    }

    protected function getEntityManager(string $serviceName): EntityManagerInterface
    {
        return $this->container->get($serviceName);
    }

    protected function getLoader(): Loader
    {
        return $this->container->get(Loader::class);
    }

    protected function getSymfonyStyle(InputInterface $input, OutputInterface $output)
    {
        return new SymfonyStyle($input, $output);
    }

    protected function getORMPurger(EntityManagerInterface $entityManager)
    {
        return new ORMPurger($entityManager);
    }

    protected function getExecutor(
        EntityManagerInterface $entityManager,
        ORMPurger $purger
    ) {
        return new ORMExecutor($entityManager, $purger);
    }
}
