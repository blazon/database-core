<?php

declare(strict_types=1);

namespace Blazon\DatabaseCore\Service;

use Doctrine\Common\DataFixtures\Loader;
use Psr\Container\ContainerInterface;

class FixtureLoaderFactory
{
    public function __invoke(ContainerInterface $container): Loader
    {
        $config = $container->get('config')['doctrine']['fixtures'] ?? [];

        $loader = new Loader();

        if (!empty($config['files'])) {
            $this->addFiles($loader, $config['files']);
        }

        if (!empty($config['dirs'])) {
            $this->addDirectories($loader, $config['dirs']);
        }

        return $loader;
    }

    public function addFiles(Loader $loader, $files)
    {
        foreach ($files as $file) {
            $loader->loadFromFile($file);
        }
    }

    public function addDirectories(Loader $loader, $dirs)
    {
        foreach ($dirs as $dir) {
            $loader->loadFromDirectory($dir);
        }
    }
}
