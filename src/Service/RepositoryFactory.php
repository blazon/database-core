<?php

declare(strict_types=1);

namespace Blazon\DatabaseCore\Service;

use Blazon\DatabaseCore\Event\FetchRepository;
use Blazon\DatabaseCore\Event\PostFetchRepository;
use Blazon\DatabaseCore\Event\PreFetchRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Repository\RepositoryFactory as DoctrineRepositoryFactory;
use Psr\Container\ContainerInterface;

class RepositoryFactory implements DoctrineRepositoryFactory
{
    /** @var EntityRepository[] */
    protected array $repositoryList = [];

    protected ContainerInterface $container;

    public function __invoke(ContainerInterface $container): RepositoryFactory
    {
        $this->container = $container;
        return $this;
    }

    public function getRepository(
        EntityManagerInterface $entityManager,
        $entityName
    ): EntityRepository {
        $repositoryHash = $entityManager->getClassMetadata($entityName)->getName() . spl_object_hash($entityManager);

        if (isset($this->repositoryList[$repositoryHash])) {
            return $this->repositoryList[$repositoryHash];
        }

        $this->firePreFetchEvent($entityManager);
        $repository = $this->repositoryList[$repositoryHash] = $this->createRepository($entityManager, $entityName);
        $this->fireFetchEvent($entityManager, $repository);
        $this->firePostFetchEvent($entityManager, $repository);

        return $repository;
    }

    protected function firePreFetchEvent(EntityManagerInterface $entityManager)
    {
        $eventManager = $entityManager->getEventManager();
        $event = new PreFetchRepository();
        $event->setEntityManager($entityManager);
        $eventManager->dispatchEvent(PreFetchRepository::EVENT_NAME, $event);
    }

    protected function fireFetchEvent(EntityManagerInterface $entityManager, EntityRepository $repository)
    {
        $eventManager = $entityManager->getEventManager();
        $event = new FetchRepository();
        $event->setEntityManager($entityManager);
        $event->setRepository($repository);
        $eventManager->dispatchEvent(FetchRepository::EVENT_NAME, $event);
    }

    protected function firePostFetchEvent(EntityManagerInterface $entityManager, EntityRepository $repository)
    {
        $eventManager = $entityManager->getEventManager();
        $event = new PostFetchRepository();
        $event->setEntityManager($entityManager);
        $event->setRepository($repository);
        $eventManager->dispatchEvent(PostFetchRepository::EVENT_NAME, $event);
    }

    protected function createRepository(EntityManagerInterface $entityManager, $entityName)
    {
        $metadata            = $entityManager->getClassMetadata($entityName);
        $repositoryClassName = $metadata->customRepositoryClassName
            ?: $entityManager->getConfiguration()->getDefaultRepositoryClassName();

        if ($this->container->has($repositoryClassName)) {
            return $this->container->get($repositoryClassName);
        }

        return new $repositoryClassName($entityManager, $metadata);
    }
}
