<?php

declare(strict_types=1);

namespace Blazon\DatabaseCore\Service;

use Doctrine\ORM\Tools\ResolveTargetEntityListener;
use Psr\Container\ContainerInterface;

class ResolveTargetEntityListenerFactory
{
    public function __invoke(ContainerInterface $container): ResolveTargetEntityListener
    {
        $map = $container->get('config')['doctrine']['resolveTargetEntities'] ?? [];

        $resolver = new ResolveTargetEntityListener();

        foreach ($map as $name => $value) {
            $resolver->addResolveTargetEntity($name, $value, []);
        }

        return $resolver;
    }
}
