<?php

declare(strict_types=1);

namespace Blazon\DatabaseCore\Service;

use Doctrine\ORM\Tools\Console\Helper\EntityManagerHelper;
use Psr\Container\ContainerInterface;

class EntityManagerHelperFactory
{
    public function __invoke(ContainerInterface $container): EntityManagerHelper
    {
        $entityManager = $container->get('doctrine.entity_manager.orm_default');
        return new EntityManagerHelper($entityManager);
    }
}
