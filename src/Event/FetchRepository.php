<?php

declare(strict_types=1);

namespace Blazon\DatabaseCore\Event;

use Doctrine\Common\EventArgs;
use Doctrine\ORM\EntityRepository;

class FetchRepository extends EventArgs
{
    use EntityManagerTrait;
    use RepositoryTrait;

    public const EVENT_NAME = 'fetchRepository';
}
