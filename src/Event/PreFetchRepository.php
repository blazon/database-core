<?php

declare(strict_types=1);

namespace Blazon\DatabaseCore\Event;

use Doctrine\Common\EventArgs;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;

class PreFetchRepository extends EventArgs
{
    use EntityManagerTrait;

    public const EVENT_NAME = 'preFetchRepository';
}
