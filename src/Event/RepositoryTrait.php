<?php

declare(strict_types=1);

namespace Blazon\DatabaseCore\Event;

use Doctrine\ORM\EntityRepository;

trait RepositoryTrait
{
    protected EntityRepository $repository;

    public function getRepository(): EntityRepository
    {
        return $this->repository;
    }

    public function setRepository(EntityRepository $repository): void
    {
        $this->repository = $repository;
    }
}
