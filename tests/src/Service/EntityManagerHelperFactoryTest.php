<?php

declare(strict_types=1);

namespace Blazon\DatabaseCore\Test\Service;

use Blazon\DatabaseCore\Service\EntityManagerHelperFactory;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Console\Helper\EntityManagerHelper;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;

/** @covers \Blazon\DatabaseCore\Service\EntityManagerHelperFactory */
class EntityManagerHelperFactoryTest extends TestCase
{
    public function testInvoke()
    {
        $mockEm = $this->createMock(EntityManagerInterface::class);
        $mockContainer = $this->createMock(ContainerInterface::class);

        $mockContainer->expects($this->any())
            ->method('get')
            ->willReturn($mockEm);

        $factory = new EntityManagerHelperFactory();

        $result = $factory($mockContainer);

        $this->assertInstanceOf(EntityManagerHelper::class, $result);
    }
}
