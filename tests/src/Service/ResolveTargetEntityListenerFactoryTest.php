<?php

declare(strict_types=1);

namespace Blazon\DatabaseCore\Test\Service;

use Blazon\DatabaseCore\Service\ResolveTargetEntityListenerFactory;
use Doctrine\ORM\Tools\ResolveTargetEntityListener;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;

/** @covers \Blazon\DatabaseCore\Service\ResolveTargetEntityListenerFactory */
class ResolveTargetEntityListenerFactoryTest extends TestCase
{
    public function testInvoke()
    {
        $mockConfig = [
            'doctrine' => [
                'resolveTargetEntities' => [
                    'some-name' => 'some-other-name'
                ]
            ]
        ];

        $mockContainer = $this->createMock(ContainerInterface::class);
        $mockContainer->expects($this->once())
            ->method('get')
            ->with($this->equalTo('config'))
            ->willReturn($mockConfig);

        $factory = new ResolveTargetEntityListenerFactory();
        $result = $factory($mockContainer);
        $this->assertInstanceOf(ResolveTargetEntityListener::class, $result);
    }
}
