<?php

declare(strict_types=1);

namespace Blazon\DatabaseCore\Test\Service;

use Blazon\DatabaseCore\Service\FixtureLoaderFactory;
use Doctrine\Common\DataFixtures\Loader;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;

/** @covers \Blazon\DatabaseCore\Service\FixtureLoaderFactory */
class FixtureLoaderFactoryTest extends TestCase
{
    public function testInvokeWithFiles()
    {
        $mockContainer = $this->createMock(ContainerInterface::class);

        $mockContainer->expects($this->any())
            ->method('get')
            ->willReturn([
                'doctrine' => [
                    'fixtures' => [
                        'files' => [
                            'test' => __DIR__ . '/../../data/fixtures/Test.php',
                        ],
                    ],
                ],
            ]);

        $factory = new FixtureLoaderFactory();

        $result = $factory($mockContainer);

        $this->assertInstanceOf(Loader::class, $result);

        $files = $result->getFixtures();

        $this->assertArrayHasKey('MyDataFixtures\Test', $files);
    }

    public function testInvokeWithDirs()
    {
        $mockContainer = $this->createMock(ContainerInterface::class);

        $mockContainer->expects($this->any())
            ->method('get')
            ->willReturn([
                             'doctrine' => [
                                 'fixtures' => [
                                     'dirs' => [
                                         'test' => __DIR__ . '/../../data/fixtures',
                                     ],
                                 ],
                             ],
                         ]);

        $factory = new FixtureLoaderFactory();

        $result = $factory($mockContainer);

        $this->assertInstanceOf(Loader::class, $result);

        $files = $result->getFixtures();

        $this->assertArrayHasKey('MyDataFixtures\Test', $files);
    }
}
