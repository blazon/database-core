<?php

declare(strict_types=1);

namespace Blazon\DatabaseCore\Test;

use Blazon\DatabaseCore\ConfigProvider;
use PHPUnit\Framework\TestCase;

/** @covers \Blazon\DatabaseCore\ConfigProvider */
class ConfigProviderTest extends TestCase
{
    public function testInvoke()
    {
        $factory = new ConfigProvider();
        $result = $factory();
        $this->assertIsArray($result);
    }
}
