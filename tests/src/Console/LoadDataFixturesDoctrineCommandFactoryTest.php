<?php

declare(strict_types=1);

namespace Blazon\DatabaseCore\Test\Console;

use Blazon\DatabaseCore\Console\LoadDataFixturesDoctrineCommand;
use Blazon\DatabaseCore\Console\LoadDataFixturesDoctrineCommandFactory;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;

/** @covers \Blazon\DatabaseCore\Console\LoadDataFixturesDoctrineCommandFactory */
class LoadDataFixturesDoctrineCommandFactoryTest extends TestCase
{
    public function testInvoke()
    {
        $mockContainer = $this->createMock(ContainerInterface::class);
        $factory = new LoadDataFixturesDoctrineCommandFactory();
        $result = $factory($mockContainer);
        $this->assertInstanceOf(LoadDataFixturesDoctrineCommand::class, $result);
    }
}
