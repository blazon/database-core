<?php

declare(strict_types=1);

namespace Blazon\DatabaseCore\Test\Console;

use Blazon\DatabaseCore\Console\LoadDataFixturesDoctrineCommand;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\Loader;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\Common\EventManager;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Sharding\PoolingShardConnection;
use Doctrine\ORM\EntityManagerInterface;
use LogicException;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;
use ReflectionMethod;
use Symfony\Component\Console\Formatter\OutputFormatterInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/** @covers \Blazon\DatabaseCore\Console\LoadDataFixturesDoctrineCommand */
class LoadDataFixturesDoctrineCommandTest extends TestCase
{
    /** @var LoadDataFixturesDoctrineCommand|MockObject */
    protected $command;

    /** @var ContainerInterface|MockObject */
    protected $mockContainer;

    /** @var InputInterface|MockObject */
    protected $mockInput;

    /** @var OutputInterface|MockObject */
    protected $mockOutput;

    /** @var EntityManagerInterface|MockObject */
    protected $mockEntityManager;

    /** @var Connection|MockObject */
    protected $mockConnection;

    /** @var Loader|MockObject */
    protected $mockLoader;

    /** @var SymfonyStyle|MockObject */
    protected $mockStyle;

    /** @var ORMPurger|MockObject */
    protected $mockPurger;

    /** @var ORMExecutor|MockObject */
    protected $mockExecutor;

    /** @var FixtureInterface|MockObject */
    protected $mockFixture;

    protected function setUp(): void
    {
        $this->mockContainer = $this->createMock(ContainerInterface::class);
        $this->mockInput = $this->createMock(InputInterface::class);
        $this->mockOutput = $this->createMock(OutputInterface::class);
        $this->mockEntityManager = $this->createMock(EntityManagerInterface::class);
        $this->mockFixture = $this->createMock(FixtureInterface::class);

        $this->mockConnection = $this->getMockBuilder(Connection::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->mockLoader = $this->getMockBuilder(Loader::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->mockStyle = $this->getMockBuilder(SymfonyStyle::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->mockPurger = $this->getMockBuilder(ORMPurger::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->mockExecutor = $this->getMockBuilder(ORMExecutor::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->command = new LoadDataFixturesDoctrineCommand($this->mockContainer);

        $this->assertInstanceOf(LoadDataFixturesDoctrineCommand::class, $this->command);
    }

    public function testConstructor()
    {
    }

    public function testConfigure()
    {
        $method = new ReflectionMethod(LoadDataFixturesDoctrineCommand::class, 'configure');
        $method->setAccessible(true);
        $method->invoke($this->command);

        $this->assertEquals('fixtures:load', $this->command->getName());
        $this->assertTrue(is_string($this->command->getDescription()));
        $this->assertTrue(is_string($this->command->getHelp()));

        $options = $this->command->getDefinition()->getOptions();

        $this->assertArrayHasKey('append', $options);
        $this->assertArrayHasKey('em', $options);
        $this->assertArrayHasKey('shard', $options);
        $this->assertArrayHasKey('purge-with-truncate', $options);
    }

    public function testExecute()
    {
        $this->command = $this->getMockBuilder(LoadDataFixturesDoctrineCommand::class)
            ->setConstructorArgs([$this->mockContainer])
            ->onlyMethods([
                'getEntityManager',
                'getLoader',
                'getSymfonyStyle',
                'getORMPurger',
                'getExecutor',
            ])->getMock();

        $this->command->expects($this->once())
            ->method('getSymfonyStyle')
            ->with(
                $this->equalTo($this->mockInput),
                $this->equalTo($this->mockOutput),
            )
            ->willReturn($this->mockStyle);

        $this->command->expects($this->once())
            ->method('getEntityManager')
            ->willReturn($this->mockEntityManager);

        $this->mockEntityManager->expects($this->any())
            ->method('getConnection')
            ->willReturn($this->mockConnection);

        $inputMap = [
            ['append', true],
            ['shard', null],
            ['em', 'entityManager'],
            ['purge-with-truncate', null],
        ];

        $this->mockInput->expects($this->any())
            ->method('getOption')
            ->willReturnMap($inputMap);

        $this->command->expects($this->once())
            ->method('getLoader')
            ->willReturn($this->mockLoader);

        $this->mockLoader->expects($this->once())
            ->method('getFixtures')
            ->willReturn(['fixtures1' => $this->mockFixture]);

        $this->command->expects($this->once())
            ->method('getORMPurger')
            ->willReturn($this->mockPurger);

        $this->mockPurger->expects($this->once())
            ->method('setPurgeMode')
            ->with(
                $this->equalTo(ORMPurger::PURGE_MODE_DELETE)
            );

        $this->command->expects($this->once())
            ->method('getExecutor')
            ->willReturn($this->mockExecutor);

        $this->mockExecutor->expects($this->once())
            ->method('setLogger')
            ->with(
                $this->isType('callable')
            );

        $this->mockExecutor->expects($this->once())
            ->method('execute')
            ->with(
                $this->equalTo(['fixtures1' => $this->mockFixture]),
                $this->equalTo(true)
            );

        $result = $this->command->run($this->mockInput, $this->mockOutput);

        $this->assertEquals(0, $result);
    }

    public function testExecuteWithPurge()
    {
        $this->command = $this->getMockBuilder(LoadDataFixturesDoctrineCommand::class)
            ->setConstructorArgs([$this->mockContainer])
            ->onlyMethods([
                              'getEntityManager',
                              'getLoader',
                              'getSymfonyStyle',
                              'getORMPurger',
                              'getExecutor',
                          ])->getMock();

        $this->mockEntityManager->expects($this->any())
            ->method('getConnection')
            ->willReturn($this->mockConnection);

        $inputMap = [
            ['append', false],
            ['shard', null],
            ['em', 'entityManager'],
            ['purge-with-truncate', null],
        ];

        $this->mockInput->expects($this->any())
            ->method('getOption')
            ->willReturnMap($inputMap);

        $this->command->expects($this->once())
            ->method('getEntityManager')
            ->willReturn($this->mockEntityManager);

        $this->command->expects($this->once())
            ->method('getLoader')
            ->willReturn($this->mockLoader);

        $this->command->expects($this->once())
            ->method('getSymfonyStyle')
            ->with(
                $this->equalTo($this->mockInput),
                $this->equalTo($this->mockOutput),
            )
            ->willReturn($this->mockStyle);

        $this->command->expects($this->once())
            ->method('getORMPurger')
            ->willReturn($this->mockPurger);

        $this->command->expects($this->once())
            ->method('getExecutor')
            ->willReturn($this->mockExecutor);

        $this->mockLoader->expects($this->once())
            ->method('getFixtures')
            ->willReturn(['fixtures1' => $this->mockFixture]);

        $this->mockPurger->expects($this->once())
            ->method('setPurgeMode')
            ->with(
                $this->equalTo(ORMPurger::PURGE_MODE_DELETE)
            );

        $this->mockExecutor->expects($this->once())
            ->method('setLogger')
            ->with(
                $this->isType('callable')
            );

        $this->mockExecutor->expects($this->once())
            ->method('execute')
            ->with(
                $this->equalTo(['fixtures1' => $this->mockFixture]),
                $this->equalTo(false)
            );

        $this->mockStyle->expects($this->once())
            ->method('confirm')
            ->willReturn(true);

        $result = $this->command->run($this->mockInput, $this->mockOutput);

        $this->assertEquals(0, $result);
    }

    public function testExecuteWithPurgeDenied()
    {
        $this->command = $this->getMockBuilder(LoadDataFixturesDoctrineCommand::class)
            ->setConstructorArgs([$this->mockContainer])
            ->onlyMethods([
                              'getEntityManager',
                              'getLoader',
                              'getSymfonyStyle',
                              'getORMPurger',
                              'getExecutor',
                          ])->getMock();

        $this->command->expects($this->once())
            ->method('getSymfonyStyle')
            ->with(
                $this->equalTo($this->mockInput),
                $this->equalTo($this->mockOutput),
            )
            ->willReturn($this->mockStyle);

        $this->command->expects($this->once())
            ->method('getEntityManager')
            ->willReturn($this->mockEntityManager);

        $this->mockEntityManager->expects($this->any())
            ->method('getConnection')
            ->willReturn($this->mockConnection);

        $this->mockConnection->expects($this->once())
            ->method('getDatabase')
            ->willReturn('database_name');

        $inputMap = [
            ['append', false],
            ['shard', null],
            ['em', 'entityManager'],
            ['purge-with-truncate', null],
        ];

        $this->mockInput->expects($this->any())
            ->method('getOption')
            ->willReturnMap($inputMap);

        $this->mockStyle->expects($this->once())
            ->method('confirm')
            ->willReturn(false);

        $this->command->expects($this->never())
            ->method('getLoader');

        $this->command->expects($this->never())
            ->method('getORMPurger');

        $this->command->expects($this->never())
            ->method('getExecutor');

        $this->mockLoader->expects($this->never())
            ->method('getFixtures');

        $this->mockPurger->expects($this->never())
            ->method('setPurgeMode');

        $this->mockExecutor->expects($this->never())
            ->method('setLogger');

        $this->mockExecutor->expects($this->never())
            ->method('execute');

        $result = $this->command->run($this->mockInput, $this->mockOutput);

        $this->assertEquals(0, $result);
    }

    public function testExecuteWithShard()
    {
        $this->command = $this->getMockBuilder(LoadDataFixturesDoctrineCommand::class)
            ->setConstructorArgs([$this->mockContainer])
            ->onlyMethods([
                              'getEntityManager',
                              'getLoader',
                              'getSymfonyStyle',
                              'getORMPurger',
                              'getExecutor',
                          ])->getMock();

        $this->command->expects($this->once())
            ->method('getSymfonyStyle')
            ->with(
                $this->equalTo($this->mockInput),
                $this->equalTo($this->mockOutput),
            )
            ->willReturn($this->mockStyle);

        $this->command->expects($this->once())
            ->method('getEntityManager')
            ->willReturn($this->mockEntityManager);

        $this->mockConnection = $this->getMockBuilder(PoolingShardConnection::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->mockEntityManager->expects($this->any())
            ->method('getConnection')
            ->willReturn($this->mockConnection);

        $inputMap = [
            ['append', true],
            ['shard', 'some-shard'],
            ['em', 'entityManager'],
            ['purge-with-truncate', null],
        ];

        $this->mockInput->expects($this->any())
            ->method('getOption')
            ->willReturnMap($inputMap);

        $this->mockConnection->expects($this->once())
            ->method('connect')
            ->with(
                $this->equalTo('some-shard')
            );

        $this->command->expects($this->once())
            ->method('getLoader')
            ->willReturn($this->mockLoader);

        $this->mockLoader->expects($this->once())
            ->method('getFixtures')
            ->willReturn(['fixtures1' => $this->mockFixture]);

        $this->command->expects($this->once())
            ->method('getORMPurger')
            ->willReturn($this->mockPurger);

        $this->mockPurger->expects($this->once())
            ->method('setPurgeMode')
            ->with(
                $this->equalTo(ORMPurger::PURGE_MODE_DELETE)
            );

        $this->command->expects($this->once())
            ->method('getExecutor')
            ->willReturn($this->mockExecutor);

        $this->mockExecutor->expects($this->once())
            ->method('setLogger')
            ->with(
                $this->isType('callable')
            );

        $this->mockExecutor->expects($this->once())
            ->method('execute')
            ->with(
                $this->equalTo(['fixtures1' => $this->mockFixture]),
                $this->equalTo(true)
            );

        $result = $this->command->run($this->mockInput, $this->mockOutput);

        $this->assertEquals(0, $result);
    }

    public function testExecuteWithShardWithInvalidShardConnection()
    {
        $this->expectException(LogicException::class);

        $this->command = $this->getMockBuilder(LoadDataFixturesDoctrineCommand::class)
            ->setConstructorArgs([$this->mockContainer])
            ->onlyMethods([
                              'getEntityManager',
                              'getLoader',
                              'getSymfonyStyle',
                              'getORMPurger',
                              'getExecutor',
                          ])->getMock();

        $this->command->expects($this->once())
            ->method('getSymfonyStyle')
            ->with(
                $this->equalTo($this->mockInput),
                $this->equalTo($this->mockOutput),
            )
            ->willReturn($this->mockStyle);

        $this->command->expects($this->once())
            ->method('getEntityManager')
            ->willReturn($this->mockEntityManager);


        $this->mockEntityManager->expects($this->any())
            ->method('getConnection')
            ->willReturn($this->mockConnection);

        $inputMap = [
            ['append', true],
            ['shard', 'some-shard'],
            ['em', 'entityManager'],
            ['purge-with-truncate', null],
        ];

        $this->mockInput->expects($this->any())
            ->method('getOption')
            ->willReturnMap($inputMap);

        $this->mockConnection->expects($this->never())
            ->method('connect');

        $this->command->expects($this->never())
            ->method('getLoader');

        $this->mockLoader->expects($this->never())
            ->method('getFixtures');

        $this->command->expects($this->never())
            ->method('getORMPurger');

        $this->mockPurger->expects($this->never())
            ->method('setPurgeMode');

        $this->command->expects($this->never())
            ->method('getExecutor');

        $this->mockExecutor->expects($this->never())
            ->method('setLogger');

        $this->mockExecutor->expects($this->never())
            ->method('execute');

        $result = $this->command->run($this->mockInput, $this->mockOutput);

        $this->assertEquals(0, $result);
    }

    public function testExecuteNoFixturesFound()
    {
        $this->command = $this->getMockBuilder(LoadDataFixturesDoctrineCommand::class)
            ->setConstructorArgs([$this->mockContainer])
            ->onlyMethods([
                              'getEntityManager',
                              'getLoader',
                              'getSymfonyStyle',
                              'getORMPurger',
                              'getExecutor',
                          ])->getMock();

        $this->command->expects($this->once())
            ->method('getSymfonyStyle')
            ->with(
                $this->equalTo($this->mockInput),
                $this->equalTo($this->mockOutput),
            )
            ->willReturn($this->mockStyle);

        $this->command->expects($this->once())
            ->method('getEntityManager')
            ->willReturn($this->mockEntityManager);

        $this->mockEntityManager->expects($this->any())
            ->method('getConnection')
            ->willReturn($this->mockConnection);

        $inputMap = [
            ['append', true],
            ['shard', null],
            ['em', 'entityManager'],
            ['purge-with-truncate', null],
        ];

        $this->mockInput->expects($this->any())
            ->method('getOption')
            ->willReturnMap($inputMap);

        $this->command->expects($this->once())
            ->method('getLoader')
            ->willReturn($this->mockLoader);

        $this->mockLoader->expects($this->once())
            ->method('getFixtures')
            ->willReturn(null);

        $this->mockStyle->expects($this->once())
            ->method('error')
            ->with(
                $this->isType('string')
            );

        $this->command->expects($this->never())
            ->method('getORMPurger');

        $this->mockPurger->expects($this->never())
            ->method('setPurgeMode');

        $this->command->expects($this->never())
            ->method('getExecutor');

        $this->mockExecutor->expects($this->never())
            ->method('setLogger');

        $this->mockExecutor->expects($this->never())
            ->method('execute');

        $result = $this->command->run($this->mockInput, $this->mockOutput);

        $this->assertEquals(1, $result);
    }

    public function testExecuteWithPurgeAndTruncate()
    {
        $this->command = $this->getMockBuilder(LoadDataFixturesDoctrineCommand::class)
            ->setConstructorArgs([$this->mockContainer])
            ->onlyMethods([
                              'getEntityManager',
                              'getLoader',
                              'getSymfonyStyle',
                              'getORMPurger',
                              'getExecutor',
                          ])->getMock();

        $this->command->expects($this->once())
            ->method('getSymfonyStyle')
            ->with(
                $this->equalTo($this->mockInput),
                $this->equalTo($this->mockOutput),
            )
            ->willReturn($this->mockStyle);

        $this->command->expects($this->once())
            ->method('getEntityManager')
            ->willReturn($this->mockEntityManager);

        $this->mockEntityManager->expects($this->any())
            ->method('getConnection')
            ->willReturn($this->mockConnection);

        $inputMap = [
            ['append', true],
            ['shard', null],
            ['em', 'entityManager'],
            ['purge-with-truncate', true],
        ];

        $this->mockInput->expects($this->any())
            ->method('getOption')
            ->willReturnMap($inputMap);

        $this->command->expects($this->once())
            ->method('getLoader')
            ->willReturn($this->mockLoader);

        $this->mockLoader->expects($this->once())
            ->method('getFixtures')
            ->willReturn(['fixtures1' => $this->mockFixture]);

        $this->command->expects($this->once())
            ->method('getORMPurger')
            ->willReturn($this->mockPurger);

        $this->mockPurger->expects($this->once())
            ->method('setPurgeMode')
            ->with(
                $this->equalTo(ORMPurger::PURGE_MODE_TRUNCATE)
            );

        $this->command->expects($this->once())
            ->method('getExecutor')
            ->willReturn($this->mockExecutor);

        $this->mockExecutor->expects($this->once())
            ->method('setLogger')
            ->with(
                $this->isType('callable')
            );

        $this->mockExecutor->expects($this->once())
            ->method('execute')
            ->with(
                $this->equalTo(['fixtures1' => $this->mockFixture]),
                $this->equalTo(true)
            );

        $result = $this->command->run($this->mockInput, $this->mockOutput);

        $this->assertEquals(0, $result);
    }

    public function testExecuteWithLogger()
    {
        $this->command = $this->getMockBuilder(LoadDataFixturesDoctrineCommand::class)
            ->setConstructorArgs([$this->mockContainer])
            ->onlyMethods([
                              'getEntityManager',
                              'getLoader',
                              'getSymfonyStyle',
                              'getORMPurger',
                              'getExecutor',
                          ])->getMock();

        $this->command->expects($this->once())
            ->method('getSymfonyStyle')
            ->with(
                $this->equalTo($this->mockInput),
                $this->equalTo($this->mockOutput),
            )
            ->willReturn($this->mockStyle);

        $this->command->expects($this->once())
            ->method('getEntityManager')
            ->willReturn($this->mockEntityManager);

        $this->mockEntityManager->expects($this->any())
            ->method('getConnection')
            ->willReturn($this->mockConnection);

        $inputMap = [
            ['append', true],
            ['shard', null],
            ['em', 'entityManager'],
            ['purge-with-truncate', null],
        ];

        $this->mockInput->expects($this->any())
            ->method('getOption')
            ->willReturnMap($inputMap);

        $this->command->expects($this->once())
            ->method('getLoader')
            ->willReturn($this->mockLoader);

        $this->mockLoader->expects($this->once())
            ->method('getFixtures')
            ->willReturn(['fixtures1' => $this->mockFixture]);

        $this->command->expects($this->once())
            ->method('getORMPurger')
            ->willReturn($this->mockPurger);

        $this->mockPurger->expects($this->once())
            ->method('setPurgeMode')
            ->with(
                $this->equalTo(ORMPurger::PURGE_MODE_DELETE)
            );

        $this->command->expects($this->once())
            ->method('getExecutor')
            ->willReturn($this->mockExecutor);

        $this->mockExecutor->expects($this->once())
            ->method('setLogger')
            ->with(
                $this->callback(function (callable $value): bool {
                    $value('my message');
                    return true;
                })
            );

        $this->mockStyle->expects($this->once())
            ->method('text')
            ->with(
                $this->equalTo('  <comment>></comment> <info>my message</info>')
            );

        $this->mockExecutor->expects($this->once())
            ->method('execute')
            ->with(
                $this->equalTo(['fixtures1' => $this->mockFixture]),
                $this->equalTo(true)
            );

        $result = $this->command->run($this->mockInput, $this->mockOutput);

        $this->assertEquals(0, $result);
    }

    public function testGetEntityManager()
    {
        $this->mockContainer->expects($this->once())
            ->method('get')
            ->with(
                $this->equalTo('em')
            )->willReturn($this->mockEntityManager);

        $method = new ReflectionMethod(
            LoadDataFixturesDoctrineCommand::class,
            'getEntityManager'
        );
        $method->setAccessible(true);

        $result = $method->invoke($this->command, 'em');

        $this->assertEquals($this->mockEntityManager, $result);
    }

    public function testGetLoader()
    {
        $this->mockContainer->expects($this->once())
            ->method('get')
            ->with(
                $this->equalTo(Loader::class)
            )->willReturn($this->mockLoader);

        $method = new ReflectionMethod(
            LoadDataFixturesDoctrineCommand::class,
            'getLoader'
        );
        $method->setAccessible(true);

        $result = $method->invoke($this->command, 'em');

        $this->assertEquals($this->mockLoader, $result);
    }

    public function testGetSymfonyStyle()
    {
        $mockFormatter = $this->createMock(OutputFormatterInterface::class);

        $this->mockOutput->expects($this->once())
            ->method('getFormatter')
            ->willReturn($mockFormatter);

        $method = new ReflectionMethod(
            LoadDataFixturesDoctrineCommand::class,
            'getSymfonyStyle'
        );
        $method->setAccessible(true);

        $result = $method->invoke(
            $this->command,
            $this->mockInput,
            $this->mockOutput
        );

        $this->assertInstanceOf(SymfonyStyle::class, $result);
    }

    public function testGetORMPurger()
    {
        $method = new ReflectionMethod(
            LoadDataFixturesDoctrineCommand::class,
            'getORMPurger'
        );
        $method->setAccessible(true);

        $result = $method->invoke(
            $this->command,
            $this->mockEntityManager
        );

        $this->assertInstanceOf(ORMPurger::class, $result);
    }

    public function testGetExecutor()
    {
        $mockEM = $this->getMockBuilder(EventManager::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->mockEntityManager->expects($this->once())
            ->method('getEventManager')
            ->willReturn($mockEM);

        $mockEM->expects($this->once())
            ->method('addEventSubscriber');

        $method = new ReflectionMethod(
            LoadDataFixturesDoctrineCommand::class,
            'getExecutor'
        );
        $method->setAccessible(true);

        $result = $method->invoke(
            $this->command,
            $this->mockEntityManager,
            $this->mockPurger
        );

        $this->assertInstanceOf(ORMExecutor::class, $result);
    }
}
