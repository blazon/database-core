<?php

declare(strict_types=1);

namespace MyDataFixtures;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Persistence\ObjectManager;

class Test implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
    }
}
